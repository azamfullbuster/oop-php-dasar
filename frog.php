<?php

class Frog extends Animal
{
    public function __construct($name, $legs = 4, $cold_blooded = false)
    {
        parent::__construct($name, $cold_blooded);
        $this->legs = $legs;
    }

    public function jump()
    {
        echo "hop hop";
    }
}
