<?php

class Elang
{
    use Hewan, Fight;

    public $legs = 2;
    public $ability = "terbang tinggi";
    public $attackPower = 10;
    public $defencePower = 5;

    public function getInfoHewan()
    {
        echo "Name : " . $this->getName();
        echo nl2br("\n");
        echo "Type : " . get_class($this);
        echo nl2br("\n");
        echo "Legs : " . $this->getLegs();
        echo nl2br("\n");
        echo "Ability : " . $this->getAbility();
        echo nl2br("\n");
        echo "Att Power : " . $this->getAttPower();
        echo nl2br("\n");
        echo "Def Power : " . $this->getDefPower();
        echo nl2br("\n");
        echo "Health : " . $this->getHealth();
        echo nl2br("\n");
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($new_name)
    {
        $this->name = $new_name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function setLegs($new_legs)
    {
        $this->legs = $new_legs;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($new_health)
    {
        $this->health = $new_health;
    }

    public function getAbility()
    {
        return $this->ability;
    }

    public function setAbility($new_ability)
    {
        $this->ability = $new_ability;
    }

    public function getAttPower()
    {
        return $this->attackPower;
    }

    public function setAttPower($new_attpower)
    {
        $this->attackPower = $new_attpower;
    }

    public function getDefPower()
    {
        return $this->defencePower;
    }

    public function setDefPower($new_defpower)
    {
        $this->defencePower = $new_defpower;
    }
}

class Harimau
{
    use Hewan, Fight;

    public $legs = 4;
    public $ability = "lari cepat";
    public $attackPower = 7;
    public $defencePower = 8;

    public function getInfoHewan()
    {
        echo "Name : " . $this->getName();
        echo nl2br("\n");
        echo "Type : " . get_class($this);
        echo nl2br("\n");
        echo "Legs : " . $this->getLegs();
        echo nl2br("\n");
        echo "Ability : " . $this->getAbility();
        echo nl2br("\n");
        echo "Att Power : " . $this->getAttPower();
        echo nl2br("\n");
        echo "Def Power : " . $this->getDefPower();
        echo nl2br("\n");
        echo "Health : " . $this->getHealth();
        echo nl2br("\n");
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($new_name)
    {
        $this->name = $new_name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function setLegs($new_legs)
    {
        $this->legs = $new_legs;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($new_health)
    {
        $this->health = $new_health;
    }

    public function getAbility()
    {
        return $this->ability;
    }

    public function setAbility($new_ability)
    {
        $this->ability = $new_ability;
    }

    public function getAttPower()
    {
        return $this->attackPower;
    }

    public function setAttPower($new_attpower)
    {
        $this->attackPower = $new_attpower;
    }

    public function getDefPower()
    {
        return $this->defencePower;
    }

    public function setDefPower($new_defpower)
    {
        $this->defencePower = $new_defpower;
    }
}
