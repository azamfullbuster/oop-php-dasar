<?php

trait Hewan
{
    public $health = 50;

    public function atraksi()
    {
        echo $this->getName() . ' sedang ' . $this->getAbility();
    }

    abstract public function getName();

    abstract public function getLegs();

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($new_health)
    {
        $this->health = $new_health;
    }

    abstract public function getAbility();
}

trait Fight
{
    use Hewan;

    public function serang($lawan)
    {
        echo $this->getName() . ' sedang menyerang ' . $lawan->getName();
    }

    public function diserang($lawan)
    {
        $this->health = $this->health - ($lawan->attackPower / $this->defencePower);
        echo $this->getName() . ' sedang diserang ';
    }

    abstract public function getAttPower();

    abstract public function getDefPower();
}
