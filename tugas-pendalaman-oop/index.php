<?php
require('parent.php');
require('character.php');

$macan = new Harimau();
$eagle = new Elang();

$macan->setName('Maungg Ganas');
$eagle->setName('Perkutut Yoi');

$macan->atraksi();
echo nl2br("\n");
$eagle->atraksi();

echo nl2br("\n");
echo nl2br("\n");

$macan->serang($eagle);
echo nl2br("\n");
$eagle->diserang($macan);

echo nl2br("\n");
echo nl2br("\n");

$eagle->serang($macan);
echo nl2br("\n");
$macan->diserang($eagle);

echo nl2br("\n");
echo nl2br("\n");

$macan->getInfoHewan();
echo nl2br("\n");
$eagle->getInfoHewan();
