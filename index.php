<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo nl2br("\n");
echo $sheep->legs; // 2
echo nl2br("\n");
echo $sheep->get_cold_blooded(); // false

echo nl2br("\n");
echo nl2br("\n");

$sungokong = new Ape("kera sakti");
echo $sungokong->name;
echo nl2br("\n");
echo $sungokong->legs;
echo nl2br("\n");
$sungokong->yell(); // "Auooo"

echo nl2br("\n");
echo nl2br("\n");

$kodok = new Frog("buduk");
echo $kodok->name;
echo nl2br("\n");
echo $kodok->legs;
echo nl2br("\n");
$kodok->jump(); // "hop hop"