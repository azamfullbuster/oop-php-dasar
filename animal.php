<?php

class Animal
{
    public $legs = 2;
    public $cold_blooded = false;
    public $name;

    public function __construct($name, $legs = 2, $cold_blooded = false)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function set_name($new_name)
    {
        $this->name = $new_name;
    }

    public function get_legs()
    {
        return $this->legs;
    }

    public function set_legs($new_legs)
    {
        $this->legs = $new_legs;
    }

    public function get_cold_blooded()
    {
        if ($this->cold_blooded == false) {
            return "false";
        } else if ($this->cold_blooded == true) {
            return "true";
        }
    }

    public function set_cold_blooded($new_blood)
    {
        $this->cold_blooded = $new_blood;
    }
}
